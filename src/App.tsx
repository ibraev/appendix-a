import React from 'react';
import './App.css';
import { Route, Routes } from "react-router-dom"
import ResultSearchPage from './pages/ResultSearchPage';
import SearchFormPage from './pages/SearchFormPage';


const App : React.FC = () => {
  return (
    <div className='container'>
      <Routes>
          <Route path='/' element={<SearchFormPage />} />
          <Route path="about" element={<ResultSearchPage />} />
      </Routes>
  </div>
  )
}

export default App;
