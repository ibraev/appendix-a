import React, { useEffect, useState } from "react";
import TextField from "../UI/TextField/TextField";
import "../FormDestionation/CreateFormDestionation.css"

const CreateFormDestionation: React.FC = () => {

    useEffect(() => {
        distanceGenerate(['Paris', 48.856614, 2.352222], ['Marseille', 43.296482, 5.369780])
    }, [])

    const [distance, setDistance] = useState(0);

    const [passenger, setPassenger] = useState(0)


    function degToRad(degrees: number): number {
        return degrees * Math.PI / 180;
      }
      

    function distanceGenerate(city1: [string, number, number], city2:[string, number, number]) {
        const R = 6371; // Радиус Земли в километрах
        const lat1 = degToRad(city1[1]);
        const lat2 = degToRad(city2[1]);
        const lon1 = degToRad(city1[2]);
        const lon2 = degToRad(city2[2]);
      
        const dlat = lat2 - lat1;
        const dlon = lon2 - lon1;
      
        const a = Math.sin(dlat / 2) ** 2 + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        //distance beeween of cities
        const distance = R * c;
              
        setDistance(distance)
    }

    const fields = [
        {
            label: "City of origin",
            id:1
        },
        {
            label: "City of destionation",
            id: 2
        }
    ]


    // this function for add new destionation
    const addDestionation = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
       return fields.push({label: "City of destionation", id: + 1})
    }

    return (
        <form className="form">
            <div className="formBox">
                {
                    fields.map((field, key) => {
                        return <TextField key={key} labelText={field.label}/>
                    })
                }
                <button className="add_destionation_button" onClick={(e) => addDestionation(e)}>Add destionation</button>
                <button className="submit_button">Submit</button>
            </div>
            <div>
            <div>
                <p>Passenngers {distance.toFixed(2)}</p>
                <div className="passengers_count">
                    <button onClick={() => setPassenger(1)} className="button">-</button>
                    <span>{passenger}</span>
                    <button className="button">+</button>
                </div>
           </div>
           <div className="date">
                <label>Date</label>
                <input className="date-input" type="date"   required/>
           </div>
            </div>
        </form>
    )
}

export default CreateFormDestionation;