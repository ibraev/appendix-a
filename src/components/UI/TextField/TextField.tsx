import React from "react";
import "../TextField/TextField.css"

interface Props  {
    labelText: string
}

const TextField: React.FC<Props> = ({labelText}) => {
    return (
        <div className="fieldBox">
            <div>
                 <label className="label_text">{labelText}</label>
            </div>
            <div>
                <input className="field" type="text" required/>
            </div>
        </div>
    )
}

export default TextField;